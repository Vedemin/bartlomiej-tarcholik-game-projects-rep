﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine;

public class SwitchScene : MonoBehaviour {

    //This is a very simple script I use in a button in the Menu scene, it is made to one day enable me to choose what scene I want to load

    public int sceneToLoad;
	
	public void Switch() {
       SceneManager.LoadScene(sceneToLoad);
    }
}