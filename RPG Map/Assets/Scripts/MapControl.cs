﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapControl : MonoBehaviour
{
    /* This is the script that generally controls the map and camera.
    It allows me to load a chosen map (project was recently started so its still WIP on at least 90% places) and then cotrol the camera */

    public Sprite assignedMap;
    public Camera mainCamera;

    //Raycasts are used to make the mouse move the camera in a controlled way
    private Ray oldCast, newCast;
    private RaycastHit oldHit, newHit;

    private SpriteRenderer map;
    private BoxCollider collider;

    [SerializeField] float scrollSensitivity = 10.0f;

    void Start()
    {
        map = GetComponent<SpriteRenderer>();
        map.sprite = assignedMap;
        collider = GetComponent<BoxCollider>();
        collider.size = map.size + new Vector2(500f, 500f);
    }

    void Update()
    {
        Zoom();
        if (Input.GetKey(KeyCode.Mouse0))
        {
            Move();
        }
        oldCast = mainCamera.ScreenPointToRay(Input.mousePosition);
    }

    private void Move()
    {
        Physics.Raycast(oldCast, out oldHit);
        newCast = mainCamera.ScreenPointToRay(Input.mousePosition);
        Physics.Raycast(newCast, out newHit);

        Vector3 offset = oldHit.point - newHit.point;

        mainCamera.transform.position = mainCamera.transform.position + offset;
    }

    private void Zoom()
    {
        //I decided that a square root of zoom has the effect I desire on zooming speed
        mainCamera.orthographicSize = mainCamera.orthographicSize + Input.GetAxisRaw("Mouse ScrollWheel") * -scrollSensitivity * Mathf.Sqrt(mainCamera.orthographicSize);
    }
}
