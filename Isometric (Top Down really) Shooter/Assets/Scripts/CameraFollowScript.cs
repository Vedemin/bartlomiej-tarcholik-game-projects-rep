﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowScript : MonoBehaviour {
    //A very simple script I created to follow the player's position with the camera from top-down view

    [SerializeField] Transform player;	

	void Update () {
        transform.position = new Vector3(player.position.x, transform.position.y, player.position.z);
	}
}
