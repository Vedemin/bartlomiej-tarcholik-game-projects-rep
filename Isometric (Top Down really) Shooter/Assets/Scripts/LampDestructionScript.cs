﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LampDestructionScript : MonoBehaviour {
    //The script was applied to every lamp and used to check if the object was hit by a bullet as well as add a point to scoring system

    [SerializeField] ScoringScript scoringSystem;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Bullet")
        {
            scoringSystem.ChangeLampAmount();
            Destroy(gameObject, 0f);
        }
    }
}
