﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour {
    [SerializeField] float lifetime;

    //I believe that this script (unlike the other)

	void Start () { //Bullet is destroyed after a certain amount of time
        Destroy(gameObject, lifetime);
    }
	
	void OnCollisionEnter (Collision objectHit) {
        Destroy(gameObject, 0f);
    }
}
