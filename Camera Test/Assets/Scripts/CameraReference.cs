﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraReference : MonoBehaviour {

    /* This script is used just to rotate the camera and lock its maximum rotation as well as to hide and locs the cursor.
    It is however outdated and a newer one was made in "Test Environment" project */

    public Transform target;
    public float cameraHeight;
    public float sensitivityY = 2.0f;
    public float sensitivityX = 2.0f;

    private float rotationY = 0.0f;
    private float rotationX = 0.0f;


	void Start () {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
	}
	
	void Update () {

        rotationY += sensitivityY * Input.GetAxis("Mouse Y");
        if (rotationY > 60)
        {
            rotationY = 60.0f;
        } else if (rotationY < -40)
        {
            rotationY = -40.0f;
        }

        rotationX += sensitivityX * Input.GetAxis("Mouse X");

            transform.eulerAngles = new Vector3(-rotationY, rotationX, 0.0f);

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.None;
        }

        transform.position = new Vector3(target.position.x, target.position.y+cameraHeight, target.position.z);
	}
}
