﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeGame : MonoBehaviour {

	float roundStartDelay = 3f;
	float startTime;
	int waitTime;
	bool RoundStarted;

	// Use this for initialization
	void Start () {
		print ("Naciśnij spację, kiedy minie czas");
		Invoke ("SetRandomTime", roundStartDelay);
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Space) && RoundStarted) {
			InputReceived ();
		}
	}

	void InputReceived() {
		RoundStarted = false;
		float playerWaitTime = Time.time - startTime;
		float error = Mathf.Abs (waitTime - playerWaitTime);

		print ("Czekałeś przez " + playerWaitTime + " sekund. Pomyliłeś się o " + error + " sekund. " + GenerateMessage(error));
		Invoke ("SetRandomTime", roundStartDelay);
	}

	string GenerateMessage (float error) {
		string message = "";
		if (error < .75f) {
			message = "Szampańsko!";
		} else if (error < 2f) {
			message = "Nieźle";
		} else {
			message = "Chujnia, panie :(";
		}
		return message;
	}

	void SetRandomTime () {
		waitTime = Random.Range (5,21);
		startTime = Time.time;
		RoundStarted = true;
		print ("Poczekaj " + waitTime + " sekund");
	}
}
