﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollower : MonoBehaviour {

	public Transform master;

	void Update () {
		transform.position = master.position;
	}
}
