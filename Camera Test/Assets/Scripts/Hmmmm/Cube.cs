﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cube : MonoBehaviour {

	public Transform sphere;

	// Use this for initialization
	void Start () {
		sphere.parent = transform;
		sphere.localScale = Vector3.one * 2;

	}
	
	// Update is called once per frame
	void Update () {
		transform.Rotate (Vector3.up * 90 * Time.deltaTime, Space.World);
		transform.Translate (Vector3.forward * Time.deltaTime * 5, Space.World);

		if (Input.GetKeyDown(KeyCode.Space)) {
			sphere.position = Vector3.zero;
		}
	}
}
