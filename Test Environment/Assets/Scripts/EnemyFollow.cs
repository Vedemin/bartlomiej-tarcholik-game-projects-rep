﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFollow : MonoBehaviour {

    /* Our starting shots at the AI, it detected if the player is in range and visible and made the AI cube move towards the player (or any target for that matter).
    The movement part was made by my friend, while I helped him and made the limited line of sight system */

    private Rigidbody rigidbody;
    private Vector3 direction;

    public Transform target; //We made the target public in case the monsters were to target something else than the player (like each other)

    [SerializeField] float viewDistance;
    [SerializeField] float movementSpeed;


    void Start () {
        rigidbody = GetComponent<Rigidbody>();	
	}


    void Update()
    {      
        transform.LookAt(target);
        transform.rotation = Quaternion.Euler(new Vector3(0,transform.rotation.eulerAngles.y,0));

        direction = (target.position - transform.position).normalized;

        Ray raycast = new Ray(transform.position, direction);

        if (Physics.Raycast(raycast, viewDistance))
        {
            Follow();
        }
    }

    private void Follow()
    {
        rigidbody.MovePosition(rigidbody.position + direction * movementSpeed * Time.deltaTime);
    }
}
