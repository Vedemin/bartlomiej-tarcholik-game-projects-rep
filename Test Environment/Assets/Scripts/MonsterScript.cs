﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterScript : MonoBehaviour
{
    /* This is our very basic script for damaging the player by the AI "monster" (a cube for test purposes),
        we decided that the AI can self-destruct after causing damage */

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            HPManagementScript hpscript = collision.gameObject.GetComponent<HPManagementScript>();

            hpscript.GetHitBro();
            Destroy(gameObject);
        }
    }
}
